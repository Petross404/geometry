#ifndef SPACE_H
#define SPACE_H

#include <utility>
#include <iostream>
#include <tuple>

class X_axis;
class Y_axis;
class Point;

struct ret_point{
	X_axis*	x_first;
	X_axis*	x_last;
	Y_axis*	y_first;
	Y_axis*	y_last;
};

class Space {};

class Width : public Space
{
public:
	Width() = default;
	Width ( double w_ ) : m_Width ( w_ ) {};
	Width ( Width const& w_ ) : m_Width ( static_cast<double> ( w_ ) ) {};
	Width& operator= ( Width&& other ) noexcept;
	~Width() = default;

	Width	width() const;
	void	set_width ( double w_ );
	void	set_width ( const Width& w_ );

	explicit operator double() const;
	Width	operator+ ( const Width& w_ );
	Width	operator- ( const Width& w_ );
private:
	double	m_Width;
};

class Length : public Space
{
public:
	Length() = default;
	Length ( double l_ ) : m_Length ( l_ ) {};
	Length ( Length const& l_ ) : m_Length ( static_cast<double> ( l_ ) ) {};
	Length& operator= ( Length&& other ) noexcept;
	~Length() = default;

	Length	length() const;
	void	set_lenght ( double l_ );
	void	set_lenght ( const Length& l_ );

	explicit operator double() const;
	Length	operator+ ( const Length& l_ ) const;
	Length	operator- ( const Length& l_ ) const;
	Length 	operator/ ( const Length& l_ ) const;

private:
	double	m_Length;
};

class X_axis : Space
{
public:
	X_axis ( double x_ ) : m_X ( x_ ) {};
	X_axis ( X_axis const& x_ ) : m_X ( static_cast<double> ( x_ ) ) {};
	X_axis ( X_axis&& x_ ) noexcept : m_X ( static_cast<double> ( x_ ) ) {};
	X_axis& operator= ( X_axis&& other ) noexcept;
	~X_axis() = default;

	explicit operator double() const;
	explicit operator Length() const;

	X_axis	operator+ ( const X_axis& x_ ) const;
	X_axis	operator- ( const X_axis& x_ ) const;
	bool	operator== (const X_axis& x_ ) const; 
	bool	operator!= (const X_axis& x_ ) const; 

protected:
	virtual X_axis&	get_x_axis() const;
	virtual void	set_x_axis ( X_axis x_ );

private:
	double	m_X;
};

class Y_axis
{
public:
	Y_axis ( double y_ ) : m_Y ( y_ ) {};
	Y_axis ( Y_axis const& y_ ) : m_Y ( static_cast<double> ( y_ ) ) {};
	Y_axis& operator= ( Y_axis&& other ) noexcept;
	~Y_axis() = default;

	explicit operator double() const;
	explicit operator Length() const;
	Y_axis	operator+ ( const Y_axis& y_ ) const;
	Y_axis	operator- ( const Y_axis& y_ ) const;
	bool	operator== (const Y_axis& y_ ) const; 
	bool	operator!= (const Y_axis& y_ ) const;

protected:
	virtual Y_axis&	get_y_axis() const;
	virtual void	set_y_axis ( Y_axis y_ );

private:
	double	m_Y;
};

class Point : public X_axis, public Y_axis
{
public:
	Point ( X_axis&& x_, Y_axis&& y_ )
		: X_axis ( std::move ( x_ ) )
		, Y_axis ( std::move ( y_ ) )
		, x ( x_ )
		, y ( y_ )
	{};

	Point ( Point& p_ )
		: X_axis ( p_.get_x_axis() )
		, Y_axis ( p_.get_y_axis() )
		, x ( p_.get_x_axis() )
		, y ( p_.get_y_axis() )
	{};

	Point& operator= ( Point&& other ) noexcept;

	~Point() = default;

	bool operator==(const Point& p_);
	bool operator!=(const Point& p_);

	X_axis&	get_x_axis() const override final;
	Y_axis&	get_y_axis() const override final;

	void	set_x_axis ( X_axis x_ ) override;
	void	set_y_axis ( Y_axis y_ ) override;
private:
	X_axis	x;
	Y_axis	y;
};

class Line : public Point
{
public:
	Line ( X_axis && x_,
		Y_axis && y_,
		X_axis && x__,
		Y_axis && y__ )
		: Point ( std::move ( x_ ), std::move ( y_ ) ),
			m_p1 ( std::move ( x_ ), std::move ( y_ ) ),
		m_p2 ( std::move ( x__ ), std::move ( y__ ) ),
		m_X1 ( x_ ),
		m_X2 ( x__ ),
		m_Y1 ( y_ ),
		m_Y2 ( y__ )
	{};

	Line ( Point && p_, Point && p__ )
		: m_X1 ( p_.get_x_axis() ),
		m_X2 ( p__.get_x_axis() ),
		m_Y1 ( p_.get_y_axis() ),
		m_Y2 ( p__.get_y_axis() ),
		Point ( std::move ( m_X1 ), std::move ( m_Y1 ) ),
		m_p1 ( std::move ( m_X1 ), std::move ( m_Y1 ) ),
		m_p2 ( std::move ( m_X2 ), std::move ( m_Y2 ) )
	{};

	virtual Length 	length() const;
	virtual double 	slope()	const;
	virtual Length 	x_len()	const;
	virtual Length 	y_len()	const;
	virtual Length 	dlt_x()	const;
	virtual Length 	dlt_y()	const;
	virtual double 	a_asin()const;

	Point 	start_point()	const;
	Point 	stop_point()	const;
	ret_point	points()	const;

private:
	X_axis	m_X1, m_X2;
	Y_axis	m_Y1, m_Y2;
	Point	m_p1, m_p2;
};
#endif // SPACE_H
