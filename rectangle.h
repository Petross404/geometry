#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "shape.h"

class Rectangle
{
public:
	Rectangle() = default;
	Rectangle( Point& p1_, Point& p2_, Point& p3_, Point& p4_ );
	Rectangle ( Line&& l1_, Line&& l2_, Line&& l3_, Line&& l4_ );


	bool	is_valid() const;
private:
	X_axis	m_X1, m_X2, m_X3, m_X4;
	Y_axis	m_Y1, m_Y2, m_Y3, m_Y4;
	Point	m_P1, m_P2, m_P3, m_P4;
	Line	m_L1, m_L2, m_L3, m_L4;
};

#endif // RECTANGLE_H
