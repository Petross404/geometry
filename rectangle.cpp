#include "rectangle.h"

#include <functional>
#include <vector>
#include <set>

Rectangle::Rectangle( Point& p1_, Point& p2_, Point& p3_, Point& p4_ )
	: m_X1( p1_.get_x_axis() )
	, m_X2( p2_.get_x_axis() )
	, m_X3( p3_.get_x_axis() )
	, m_X4( p4_.get_x_axis() )
	, m_Y1( p1_.get_y_axis() )
	, m_Y2( p2_.get_y_axis() )
	, m_Y3( p3_.get_y_axis() )
	, m_Y4( p4_.get_y_axis() )
	, m_P1( std::move( m_X1 ), std::move( m_Y1 ) )
	, m_P2( std::move( m_X2 ), std::move( m_Y2 ) )
	, m_P3( std::move( m_X3 ), std::move( m_Y3 ) )
	, m_P4( std::move( m_X4 ), std::move( m_Y4 ) )
	, m_L1( std::move( m_P1 ), std::move( m_P2 ) )
	, m_L2( std::move( m_P2 ), std::move( m_P3 ) )
	, m_L3( std::move( m_P3 ), std::move( m_P4 ) )
	, m_L4( std::move( m_P4 ), std::move( m_P1 ) )
{}

Rectangle::Rectangle( Line&& l1_, Line&& l2_, Line&& l3_, Line&& l4_ )
	: m_X1( l1_.start_point().get_x_axis() )
	, m_X2( l1_.stop_point().get_x_axis() )
	, m_X3( l3_.start_point().get_x_axis() )
	, m_X4( l3_.stop_point().get_x_axis() )
	, m_Y1( l1_.start_point().get_y_axis() )
	, m_Y2( l1_.stop_point().get_y_axis() )
	, m_Y3( l3_.start_point().get_y_axis() )
	, m_Y4( l3_.stop_point().get_y_axis() )
	, m_P1( l1_.start_point() )
	, m_P2( l2_.start_point() )
	, m_P3( l3_.start_point() )
	, m_P4( l4_.start_point() )
	, m_L1( std::move( *l1_.points().x_first ), std::move( *l1_.points().y_first ),
		std::move( *l1_.points().x_last ), std::move( *l1_.points().y_last ) )
	, m_L2( std::move( *l2_.points().x_first ), std::move( *l2_.points().y_first ),
		std::move( *l2_.points().x_last ), std::move( *l2_.points().y_last ) )
	, m_L3( std::move( *l3_.points().x_first ), std::move( *l3_.points().y_first ),
		std::move( *l3_.points().x_last ), std::move( *l3_.points().y_last ) )
	, m_L4( std::move( *l4_.points().x_first ), std::move( *l4_.points().y_first ),
		std::move( *l4_.points().x_last ), std::move( *l4_.points().y_last ) )
{}

bool Rectangle::is_valid() const
{
	std::vector<Line&> v_{
				&const_cast<Line&>(m_L1), 
				&const_cast<Line&>(m_L2), 
				&const_cast<Line&>(m_L3), 
				&const_cast<Line&>(m_L4)
			};

}


// kate: indent-mode cstyle; indent-width 8; replace-tabs off; tab-width 8; 
