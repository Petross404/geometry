#include <iostream>

#include "rectangle.h"
#include "functions.h"

int main()
{
	Point p( X_axis( 3 ), Y_axis( 4 ) );
	Point p2( X_axis( 10 ), Y_axis( 4 ) );
	Point p3( 4, 6 );

	Line l1( Point( 0, 0 ), Point( 0, 10 ) );
	Line l2( Point( 0, 10 ), Point( 10, 10 ) );
	Line l3( Point( 10, 10 ), Point( 10, 0 ) );
	Line l4( Point( 10, 0 ), Point( 0, 0 ) );

	Rectangle r1(l1, l2, l3, l4);

	std::cout << static_cast<double>( l2.length() ) << std::endl
		  << static_cast<double>( l2.slope() ) << std::endl
		  << static_cast<double>( l2.a_asin() ) << std::endl;

	return 0;
}
// kate: indent-mode cstyle; indent-width 8; replace-tabs off; tab-width 8; 
