#include "space.h"
#include <cmath>

Width& Width::operator= ( Width&& other ) noexcept
{
	if ( this != &other )
		std::swap( m_Width, other.m_Width );

	return *this;
}

Width Width::width() const
{
	return m_Width;
}

void Width::set_width( double w_ )
{
	m_Width = w_;
}

void Width::set_width( Width const& w_ )
{
	m_Width = static_cast<double>( w_ );
}

Width::operator double() const
{
	return m_Width;
}

Width Width::operator+ ( const Width& w_ )
{
	Width width_{};
	width_.m_Width = this->m_Width + w_.m_Width;
	return width_;
}

Width Width::operator- ( const Width& w_ )
{
	Width width_{};
	width_.m_Width = this->m_Width + w_.m_Width;
	return width_;
}

Length Length::length() const
{
	return m_Length;
}

void Length::set_lenght( double l_ )
{
	m_Length = l_;
}

void Length::set_lenght( const Length& l_ )
{
	m_Length = static_cast<double>( l_ );
}

Length& Length::operator= ( Length&& other ) noexcept
{
	if ( this != &other )
		std::swap( m_Length, other.m_Length );

	return *this;
}

Length::operator double() const
{
	return m_Length;
}

Length Length::operator+ ( const Length& l_ ) const
{
	Length length_{};
	length_.m_Length = this->m_Length + l_.m_Length;
	return length_;
}

Length Length::operator- ( const Length& l_ ) const
{
	Length length_{};
	length_.m_Length = this->m_Length - l_.m_Length;
	return length_;
}

Length Length::operator/ ( const Length& l_ ) const
{
	Length length_{};
	length_.m_Length = this->m_Length / l_.m_Length;
	return length_;
}

X_axis& X_axis::get_x_axis() const
{
	auto x = static_cast<X_axis>( this->m_X );
	return *( std::addressof( x ) );
}

void X_axis::set_x_axis( X_axis x_ )
{
	this->m_X = static_cast<double>( x_ );
}

X_axis& X_axis::operator= ( X_axis&& other ) noexcept
{
	if ( this != &other )
		std::swap( m_X, other.m_X );

	return *this;
}

X_axis::operator double() const
{
	return this->m_X;
}

X_axis::operator Length() const
{
	return this->m_X;
}

X_axis X_axis::operator+ ( const X_axis& x_ ) const
{
	X_axis* x_axis = nullptr;
	x_axis->m_X = this->m_X + x_.m_X;
	return *x_axis;
}

X_axis X_axis::operator- ( const X_axis& x_ ) const
{
	X_axis* x_axis = nullptr;
	x_axis->m_X = this->m_X - x_.m_X;
	return *x_axis;
}

bool X_axis::operator==( const X_axis& x_ ) const
{
	return static_cast<double>(m_X) == static_cast<double>(x_);
}

bool X_axis::operator!=( const X_axis& x_ ) const
{
	return static_cast<double>(m_X) != static_cast<double>(x_);
}

Y_axis& Y_axis::operator= ( Y_axis&& other ) noexcept
{
	if ( this != &other )
		std::swap( m_Y, other.m_Y );

	return *this;
}

Y_axis::operator double() const
{
	return this->m_Y;
}

Y_axis::operator Length() const
{
	return this->m_Y;
}

Y_axis& Y_axis::get_y_axis() const
{
	auto y = static_cast<Y_axis>( this->m_Y );
	return *( std::addressof( y ) );
}

Y_axis Y_axis::operator+ ( const Y_axis& y_ ) const
{
	Y_axis* y_axis = nullptr;
	y_axis->m_Y = this->m_Y + y_.m_Y;
	return *y_axis;
}

Y_axis Y_axis::operator- ( const Y_axis& y_ ) const
{
	Y_axis* y_axis = nullptr;
	y_axis->m_Y = this->m_Y - y_.m_Y;
	return *y_axis;
}

bool Y_axis::operator==( const Y_axis& y_ ) const
{
	return static_cast<double>(m_Y) == static_cast<double>(y_);
}

bool Y_axis::operator!=( const Y_axis& y_ ) const
{
	return static_cast<double>(m_Y) != static_cast<double>(y_);
}

void Y_axis::set_y_axis( Y_axis y_ )
{
	m_Y = static_cast<double>( y_ );
}

X_axis& Point::get_x_axis() const
{
	return X_axis::get_x_axis();
}

Y_axis& Point::get_y_axis() const
{
	return Y_axis::get_y_axis();
}

void Point::set_x_axis( X_axis x_ )
{
	X_axis::set_x_axis( x_ );
}

void Point::set_y_axis( Y_axis y_ )
{
	Y_axis::set_y_axis( y_ );
}

Length Line::length() const
{
	X_axis	dx = m_X2 - m_X1;
	Y_axis	dy = m_Y2 - m_Y1;
	double	d_ = pow( ( static_cast<double>( dx ) ), 2 ) + pow( ( static_cast<double>( dy ) ), 2 );
	return sqrt( d_ ); 	// return distance as length
}

double Line::slope() const
{
	X_axis	dx = m_X2 - m_X1;
	Y_axis	dy = m_Y2 - m_Y2;

	return static_cast<double>( dy ) / static_cast<double>( dx );
}

Length Line::x_len() const
{
	return static_cast<Length>( m_X2 - m_X1 );
}

Length Line::y_len() const
{
	return static_cast<double>( m_Y2 ) - static_cast<double>( m_Y1 );
}

double Line::a_asin() const
{
	double sin = static_cast<double>( this->length() / this->y_len() );
	double a = asin( sin );
	return  a;
}

Length Line::dlt_x() const
{
	return static_cast<Length>( m_X2 - m_X1 );
}

Length Line::dlt_y() const
{
	return static_cast<Length>( m_Y2 - m_Y1 );
}

Point& Point::operator= ( Point&& other ) noexcept
{
	if ( this != &other )
	{
		std::swap( x, other.x );
		std::swap( y, other.y );
	}
	
	return *this;
}

bool Point::operator==( const Point& p_ )
{
	return ( this->x == p_.get_x_axis() && this->y == p_.get_y_axis() );
}

bool Point::operator!=( const Point& p_ )
{
	return ( this->x != p_.get_x_axis() || this->y != p_.get_y_axis() );
}

Point Line::start_point() const
{
	return const_cast<Point&>( m_p1 );

}

Point Line::stop_point() const
{
	return const_cast<Point&>( m_p2 );
}

ret_point Line::points() const
{
	ret_point* r = nullptr;

	r->x_first 	= static_cast<X_axis*>( std::addressof( m_p1.get_x_axis() ) );
	r->x_last 	= static_cast<X_axis*>( std::addressof( m_p2.get_x_axis() ) );
	r->y_first 	= static_cast<Y_axis*>( std::addressof( m_p1.get_y_axis() ) );
	r->y_last 	= static_cast<Y_axis*>( std::addressof( m_p2.get_y_axis() ) );

	return *r;
}

// kate: indent-mode cstyle; indent-width 8; replace-tabs off; tab-width 8; 
